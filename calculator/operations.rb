require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      data = JSON.parse(grades)
      ignored = blacklist.split(" ")
      div = data.count
      remembered = data.select do |test|
        not ignored.include? test
      end

      resultado = 0
      remembered.each do |name, grade|
        resultado += grade 
      end
      
      div = remembered.size 
      resultado = resultado.to_f/div.to_f
      return resultado
    end

    def no_integers(numbers)
      lea = numbers.split(" ")
      resultado = ""
      lea.each do |numero|
        if divisivel(numero)
          resultado += "S "
        else
          resultado += "N "
        end
      end
      return resultado
    end

    def divisivel(num)
      var1 = num[-1]
      var2 = num[-2]
        if var1 == "0" || var1 == "5"
          if var2 == "0" || var2 == "2" || var2 == "5" || var2 == "7"
            return true
          else 
            return false
          end
        else 
          return false
        end
    end
  
    def filter_films(genres)
      films = get_films
      entries = genres.split" "
      ano = gets.chomp
      resultado = Array.new

      entries.each do |movie|
        films[:movies].select do |genre|
          if (genre[:genres].include? movie and genre[:year] >= ano)
            resultado << genre[:title]
          end
        end
      end
      return resultado
    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
