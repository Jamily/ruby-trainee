require_relative 'operations'

module Calculator
  class Menu
    def initialize
      operations = Operations.new
      puts "-----------------------"
      puts 'Bem vindo a calculadora'
      puts "-----------------------"
      puts '1 - Média preconceituosa'
      puts '2 - Calculadora sem número'
      puts '3 - Filtrar filmes'
      puts '0 - Sair'
      print 'Opção: '
      
      operations = gets.chomp.to_i
      case operations
        when 1
          puts "Digite os nomes e notas dos alunos: "
          lista = gets.chomp
          puts "Digite os nomes dos alunos que não vão entrar na média: "
          blacklist = gets.chomp
          resultado = Operations.new.biased_mean(lista,blacklist)
          system "clear"
          puts resultado
        when 2
          print 'Digite um número: '
          numbers = gets.chomp
          resultado = Operations.new.no_integers(numbers)
          puts resultado
        when 3
          print 'Digite o(s) gênero(s) dos filmes: '
          genres = gets.chomp
          print 'Digite a partir de qual ano: '
          resultado = Operations.new.filter_films(genres)
          
          if resultado.empty?
            system "clear"
            puts "Não foi encontrado nenhum filme"
          else 
            system "clear"
            puts resultado
          end
        when 0
          exit
      end
        
    end
  end
end
